import path from 'path'

export const routes = [
  {
    name: 'tmp-app-builder-page',
    path: '/tmp-app-builder-page',
    component: path.resolve(__dirname, 'pages/tmpPageBuilder.vue'),
  },
]
